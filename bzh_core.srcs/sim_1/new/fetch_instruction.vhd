----------------------------------------------------------------------------------
-- Company: UBS
-- Engineer: Pierre Fontaine
-- 
-- Create Date: 28.12.2019 19:18:41
-- Design Name: 
-- Module Name: fetch_instruction - Behavioral
-- Project Name: bzh_core
-- Target Devices: 
-- Tool Versions: 2019.1
-- Description: This component wrap each components for the first stage of our core
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fetch_instruction is
    generic (
        N : integer;
        N_OP : integer;
        N_AD : integer
    );
    port (
        clk : in std_logic;
        pc_out : out std_logic_vector(N - 1 downto 0);
        op_out : out std_logic_vector(N_OP - 1 downto 0);
        ri_out, rj_out, rk_out : out std_logic_vector(N_AD - 1 downto 0)
    );
end fetch_instruction;

architecture Behavioral of fetch_instruction is
    -- Here are added the component used for the first stage
    
    component add is
        generic (N : integer);
        Port (
            clk, ce : in std_logic;
            op_a, op_b : in std_logic_vector(N downto 0);
            res : out std_logic_vector(N downto 0) 
        );
    end component;
    
    component program_counter is
        Generic (N : integer);
        Port (
            clk, ce : in std_logic;
            offset_in : in std_logic_vector(N downto 0);
            offset_out : out std_logic_vector(N downto 0)
        );
    end component; 
    
    component mux_2 is 
        generic (N : integer);
        port (
            clk, ce : in std_logic;
            sel     : in std_logic;
            A, B    : in std_logic_vector(N downto 0);
            res     : out std_logic_vector(N downto 0)
        );
    end component;
    
    component mem_instruction is
        generic (
            N : integer;
            N_OP : integer;
            N_AD : integer
        );
        port ( 
            clk, re                 : in std_logic;
            addr_r                  : in std_logic_vector(N-1 downto 0);
            op_out                  : out std_logic_vector(N_OP-1 downto 0);
            ri_out, rj_out, rk_out  : out std_logic_vector(N_AD-1 downto 0)
        );
    end component;
    
    -- Here are added signals used for linking the different components
    
begin

    cp_d : generic map () port map ();


end Behavioral;
