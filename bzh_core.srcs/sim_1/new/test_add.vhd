----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 28.12.2019 17:32:28
-- Design Name: 
-- Module Name: test_add - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity test_add is
end test_add;

architecture Behavioral of test_add is

    component add is
        generic (N : integer);
        Port (
            clk, ce : in std_logic;
            op_a, op_b : in std_logic_vector(N downto 0);
            res : out std_logic_vector(N downto 0) 
        );
    end component;
    
    signal clk_stim : std_logic := '0';
    signal ce_stim  : std_logic := '1';
    signal op_a_stim : std_logic_vector(7 downto 0) := x"00";
    signal op_b_stim : std_logic_vector(7 downto 0) := x"00";
    signal res_stim : std_logic_vector(7 downto 0);
    signal num_test : std_logic_vector(7 downto 0) := x"00";

begin
    
    clk_stim <= not clk_stim after 10 ns;
    
    process (clk_stim)
    begin
        if rising_edge(clk_stim) then
            case num_test is
            when x"00" =>
                op_a_stim <= x"01";
                op_b_stim <= x"02";
                num_test  <= x"01";
            when x"01" =>
                op_a_stim <= x"0F";
                op_b_stim <= x"0F";
                num_test  <= x"02";
            when x"02" =>
                op_a_stim <= x"FF";
                op_b_stim <= x"FF";
                num_test  <= x"03";
            when OTHERS =>
            end case;
        end if;
    end process;
    
    dut : add generic map (N => 7) port map (
        clk => clk_stim,
        ce => ce_stim,
        op_a => op_a_stim,
        op_b => op_b_stim,
        res => res_stim
    );

end Behavioral;
