----------------------------------------------------------------------------------
-- Company: UBS
-- Engineer: Pierre Fontaine
-- 
-- Create Date: 28.12.2019 16:57:44
-- Design Name: 
-- Module Name: top_core - Behavioral
-- Project Name: bzh_core 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux_2 is
    generic (N : integer);
    port (
        clk, ce : in std_logic;
        sel     : in std_logic;
        A, B    : in std_logic_vector(N downto 0);
        res     : out std_logic_vector(N downto 0)
    );
end mux_2;

architecture Behavioral of mux_2 is
begin
    process ( clk )
    begin
        if rising_edge(clk) and ce = '1' then
            case sel is
                when '0' =>
                    res <= A;
                when '1' =>
                    res <= B;
                when Others =>
            end case;
        end if;
    end process;
end Behavioral;
