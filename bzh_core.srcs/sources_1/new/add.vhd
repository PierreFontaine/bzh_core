----------------------------------------------------------------------------------
-- Company: UBS
-- Engineer: Pierre Fontaine
-- 
-- Create Date: 28.12.2019 16:53:13
-- Design Name: add
-- Module Name: add - Behavioral
-- Project Name: bzh_core
-- Target Devices: 
-- Tool Versions: 2019.1
-- Description: This adder can add two operands. 
-- Data size is chosen at the instance via the generic keyword.
-- 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity add is
    generic (N : integer);
    Port (
        clk, ce : in std_logic;
        op_a, op_b : in std_logic_vector(N downto 0);
        res : out std_logic_vector(N downto 0) 
    );
end add;

architecture Behavioral of add is
begin
    main : process ( clk )
    begin
        if rising_edge ( clk ) then
            if ce = '1' then
                res <= std_logic_vector(unsigned(op_a) + unsigned(op_b));
            end if;
        end if;
    end process;
end Behavioral;
