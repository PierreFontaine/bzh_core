----------------------------------------------------------------------------------
-- Company: UBS
-- Engineer: Pierre Fontaine
-- 
-- Create Date: 28.12.2019 19:20:12
-- Design Name: 
-- Module Name: program_counter - Behavioral
-- Project Name: bzh_core
-- Target Devices: 
-- Tool Versions: 
-- Description: Program counter, act as a simple register
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity program_counter is
    Generic ( N : integer);
    Port (
        clk, ce : in std_logic;
        offset_in : in std_logic_vector(N downto 0);
        offset_out : out std_logic_vector(N downto 0)
    );
end program_counter;

architecture Behavioral of program_counter is
begin
    offset_out <= offset_in;
end Behavioral;
