----------------------------------------------------------------------------------
-- Company: UBS
-- Engineer: Pierre Fontaine
-- 
-- Create Date: 28.12.2019 16:57:44
-- Design Name: 
-- Module Name: mem_instruction
-- Project Name: bzh_core
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mem_instruction is
    generic (
        N : integer;
        N_OP : integer;
        N_AD : integer
    );
    port ( 
        clk, re                 : in std_logic;
        addr_r                  : in std_logic_vector(N-1 downto 0);
        op_out                  : out std_logic_vector(N_OP-1 downto 0);
        ri_out, rj_out, rk_out  : out std_logic_vector(N_AD-1 downto 0)
    );
end mem_instruction;

architecture Behavioral of mem_instruction is
    subtype ligne is std_logic_vector((N_OP + N_AD + N_AD)-1 downto 0);
    type mem_array is array ((N**2)-2 downto 0) of ligne;
    signal memory : mem_array;
begin
    process (clk)
    begin
        if rising_edge(clk) and re = '1' then
            op_out <= memory(to_integer(unsigned(addr_r)))((N_OP + N_AD + N_AD + N_AD)-1 downto (N_AD + N_AD + N_AD));
            ri_out <= memory(to_integer(unsigned(addr_r)))((N_AD + N_AD + N_AD)-1 downto (N_AD + N_AD));
            rj_out <= memory(to_integer(unsigned(addr_r)))((N_AD + N_AD)-1 downto (N_AD));
            rk_out <= memory(to_integer(unsigned(addr_r)))(N_AD -1 downto 0);
        end if;
    end process;
end Behavioral;
