# BZH_CORE

## Purpose

This project aims to create a small and simple core using VHDL. Design are tested on Basys3 development board and are created using Vivado 2019.1.

## Design Flow

First, let's create and test basic components such as adder, multiplexer, decoder ... Then we will work on how to make it more efficient and cable evrything. 

### [STEP 1] Fetch Instruction Stage

### [STEP 2] Decode Instruction Stage

### [STEP 3] Execute Stage

## Side Idea

It would be great to integrate continuous test using Cocotb or Vunit and automate evrything on each commit !
Also, a documentation should be wrote describing each component at first.

